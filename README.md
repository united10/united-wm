# united-wm

modos to wm

1. install awesome wm in your distro.

2. install "ttf-font-awesome" package for manjaro and "fonts-font-awesome" for ubuntu.

3. install comic sans ms font

4. install nerd-fonts-noto-sans-mono for alacritty fonts

5. install noto-fonts-emoji for emoji font

##Directions

- git clone --recursive https://gitlab.com/united10/united-wm.git

- mkdir ~/.config/awesome if not present

- mv -bv united-wm/* ~/.config/awesome && rm -rf united-wm

- rsync -arv --exclude='- GTK-settings/' --exclude='- /README.rst' --exclude='- /custom.cfg' --exclude=-' /wm-programs.txt' --delete-excluded united-wm/* ~/.config/awesome

or
- rsync -rvP --exclude={GTK-settings/,/README.rst,/custom.cfg,/wm-programs.txt} --delete-excluded united-wm/* $HOME/.config/awesome

PROGRAMS USED

- dwm and dmenu(windowmanager and app launcher), htop(system monitor), flameshot(screen capturing) or maim, pcmanfm or thunar(graphical file manager), sxiv(minimal image viewer), ranger(terminal file manager), alacritty or st (terminal emulator), xdman(download manager) or youtube-dl plus ffmpeg, engrampa & xarchiver(archive manager), vim or nvim(commandline text editor), gvim(graphical text editor), redshift(display color temperature ), keepassxc or pass(password manager), Arandr or xrandr(graphical screen renderer), gparted or fdisk(disk manager),nm-applet(network manager), volumeicon(graphical volume icon), pulseaudio pusleaudio-alsa(audio),pavucontrol(graphical pulseaudio control), bluez & bluez-utils & pulseaudio-bluetooth & blueman(bluetooth manager), lxsession(lxpolkit(polkit agent)), lxappearance or xfce4-settings-manager(settings manager), widget or gtk theme(Dracular), icon theme(Dracular), qpdfview or zathura(pdf viewer), gcolor2(color picker), unclutter(to remove idle cursor on display),xclip or xsel(copy paste tool), slock(simple screen locker), picom(compositor) or xcompmgr, libreoffice(office packages), brave (browser), galculator or bc(gtk calculator & terminal calculator), pamixer and pavucontrol(terminal & GUI mixer), cbatticon(for battery notification), xsane (scanning program), printing (cups and system-config-printer), tlp,tlpui or tlpui-git and autocpu-freq (power saving utilities), zint-qt (qr codes), qbittorrent (qt torrenting application), timeshift (backuptool), vlc or mpv (video player), mpd mpc and ncmpcpp(daemon and music player), neomutt(terminal email client), dunst(notificaton server), light (aur light controller)

- FOR GTK THEMES if GTK3 not installed install it

   - GTK 2 user specific: ~/.gtkrc-2.0
   - GTK 2 system wide: /etc/gtk-2.0/gtkrc
   - GTK 3 user specific: $XDG_CONFIG_HOME/gtk-3.0/settings.ini, or $HOME/.config/gtk-3.0/settings.ini if $XDG_CONFIG_HOME is not set
    -GTK 3 system wide: /etc/gtk-3.0/settings.ini

- incase thunar is installed go to edit,configure custome actions and change Command and Shortcut

- for copy paste from vim to vim in Visual mode

	- "*dd --cut a line (3dd for three lines)
	- "*yy --copy a line (3yy for three lines)
	- "*p --paste lines on line after cursor
	- "*P --paste line on line before cursor


- to force qt apps to use gtk theme

	- add the following lines to /etc/environment
		 - QT_QPA_PLATFORMTHEME=gtk2
	 	- QT_STYLE_OVERIDE=gtk2

- to change notifications to dunst

	- as super user (sudo vim /usr/share/dbus-1/services/org.knopwob.dunst.service)

		-add  "-conf /home/united/.config/dunst/dunstrc"


- to use grub in artix create cutome.cfg and put it in /boot/grub and add -5.4-   or curreent kernel in first two

- to unblock wifi on artix install
 		-sudo vim /var/lib/connman/settings
		*set values required to true


- for xcursor-breeze on manjaro

- on arch aur breeze-snow-cursor-theme

- for system-wide configuration, one can edit /usr/share/icons/default/index.theme

- ~/.icons/default/index.theme
			- [icon theme]
			- Inherits=cursor_theme_name

- ~/.config/gtk-3.0/settings.ini
			- [Settings]

			- gtk-cursor-theme-name=cursor_theme_name

- for tap on click			-
- /etc/X11/xorg.conf.d/40-libinput.conf

Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
	Option "DisableWhileTyping" "on"
        #Option "ScrollMethod" "edge"
	# Enable left mouse button by tapping
	Option "Tapping" "on"
EndSection

- for tearfree
   /etc/X11/xorg.conf.d/20-intel-gpu.conf (name doesnt matter)
        Section 	"Device"
			Identifier "Intel graphics"
			Driver	   "intel"
			Option 	   "TearFree" "true"
 		#	Option     "AccelMethod" "uxa"
EndSection
	-## can replace Driver "intel" with "amdgpu" or "randeon"
- for hidden wifi connection
	- nmcli c add type wifi con-name {connection name} ifname wlan0 ssid {ssid}
	- nmcli dev wifi connect {ssid} password {password} hidden yes
	- nmcli c delete {connection name}
